import { transition, style, animate, state, trigger } from "@angular/animations";
export const dropdownAnim = trigger('dropdownAnim', [
    state('void', style({
        transform: 'translate(0,100%) scale(.3)',
        opacity: 0
    })),
    transition('* => void', [
        animate(200, style({
            transform: 'translate(0,100%) scale(.3)',
            opacity: 0,
        }))]
    ),
    transition('void => *', [
        animate(230, style({
            transform: 'translate(0) scale(1)',
            opacity: 1
        }))
    ]),
]);