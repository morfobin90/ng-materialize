import { transition, style, animate, trigger, query, stagger, group, state } from "@angular/animations";
export const easeOutSinFn = 'cubic-bezier(0.390, 0.575, 0.565, 1.000)';
export const easeInSinFn = 'cubic-bezier(0.470, 0.000, 0.745, 0.715)';
export const listAnimation = [ trigger('itemListAnimation', [
    transition('* => *', [
        query(':enter',
            [style({
                transform: 'translateY(-100px) scaleY(0)',
                opacity: 0,
            }),
            stagger(40,
                [
                    animate('140ms ' + easeInSinFn,
                        style({
                            transform: 'translateY(0) scaleY(1)',
                            opacity: 1,
                        })
                    )
                ]
            )]
            , { optional: true }),
        query(':leave', [
            stagger('50ms', [
                group([
                    animate('230ms ' + easeOutSinFn, style({
                        transform: 'translateY(-100px) scaleY(0)',
                    })),
                    animate('130ms ' + easeOutSinFn, style({
                        opacity: 0,
                    }))
                ])

            ])
        ], { optional: true })
    ], )
])]