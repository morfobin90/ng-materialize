import { Directive, ElementRef, Renderer2, OnInit, ViewChildren, QueryList, AfterViewInit, ContentChildren } from '@angular/core';
import { OptionDirective } from './option.directive';

@Directive({
  selector: '[ngModel][ngzSelect]',

})
export class SelectDirective implements AfterViewInit , OnInit {

  constructor(private el :ElementRef,
              private rendrer: Renderer2) { }

    @ContentChildren(OptionDirective) options: QueryList<OptionDirective>;
    ngAfterViewInit() {
    this.rendrer.setStyle(this.el.nativeElement,'display','none');
    this.makeOptionsDom(this.options.map( item => item.text))
    // console.log(this.options);



  }
  private makeOptionsDom(options){
    options.forEach(option => {
      this.el.nativeElement.append('<div>' + option + '</div>')
    });
  }
  ngOnInit(): void {

  }
}
