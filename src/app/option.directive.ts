import { Directive, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[ngzOption]'
})
export class OptionDirective implements AfterViewInit {

  constructor(private el :ElementRef) { }
  value: string;
  text:string;
  ngAfterViewInit() {
    this.value = this.el.nativeElement.value;
    this.text = this.el.nativeElement.innerHTML;
  }
}
