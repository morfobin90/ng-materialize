import { Component, OnInit, Input, EventEmitter, Output, HostListener, ViewChild, ElementRef, AfterViewChecked, AfterViewInit, OnChanges } from '@angular/core';
import * as SimpleBar from 'simplebar';

import { listAnimation } from '../animations/list';

@Component({
  selector: 'ngz-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  animations: listAnimation
})
export class DropdownComponent implements OnInit {

  constructor() { }
  listShown: boolean;
  selectedItem: any;
  @Input() label;
  @Input() items: Array<any>;
  @Output() onItemSelected: EventEmitter<any> = new EventEmitter<any>();
  @HostListener('blur') onTriggerBlur() {
    this.listShown = false
  }
  @ViewChild('itemsList') private itemsDOM: ElementRef;
  selectItem(item: any) {
    this.listShown = false
    this.selectedItem = item;
    this.onItemSelected.emit(item);
    this.label = this.selectedItem.text;
  }
  showList() {
    this.listShown = true
  }
  ngOnInit() {
    if (this.itemsDOM) {
      new SimpleBar(this.itemsDOM.nativeElement)
    }
    this.label = this.label || 'Show Options'
    this.listShown = false;
  }


}
